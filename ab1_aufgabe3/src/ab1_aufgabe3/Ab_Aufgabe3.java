// Kebelmann 25.09.2020 Aufgabe 3 zum Arbeitsblatt eins
package ab1_aufgabe3;

public class Ab_Aufgabe3 {

	public static void main(String[] args) {
		System.out.println("__________________________");
		System.out.printf("|%-12s| %10s|\n", "Fahrenheit" , "Celsius");
		System.out.println("--------------------------");
		System.out.printf("|%-12d| %10.2f|\n" , -20 ,  -28.8889);
		System.out.printf("|%-12d| %10.2f|\n" , -10 ,  -23.3333);
		System.out.printf("|%-12d| %10.2f|\n" , 20 ,  -17.7778);
		System.out.printf("|%-12d| %10.2f|\n" , 30 ,  -1.1111);
	}

}
